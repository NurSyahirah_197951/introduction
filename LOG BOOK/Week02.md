WEEKLY REPORT_ WEEK 2
| Items | Description |
| ------ | ------ |
|Name| Nur Syahirah |
| Matrics No | 197951 |
| Subsystem | Payload Design(Sprayer) |
| Group | 5 |
| Date | 5 November 2021 |

| header | header |
| ------ | ------ |
| What is our agenda this week and what are our/my goals? | Our goal for this week is struggling with our Gantt Chart so that we can follow the timeline with correct milestones. As for the agenda this week we are discussing about details of our subsystem such as parameters of payload, the system of sprayer and many more.  |
| What discussion did you/your team make in solving a problem? | Since our group has to design sprayer, we had a problem where we need to find out the suitable shape for water tank. How we solving the problem is by deciding to visit a company that does drones with payload. we decide to go to the company on the next week.  |
| How did you/your team make those decisions (method) | One of our teammates ever intern in that company. So she suggest our group to look by drones that they use to spray crops using payload which is sprayer system.  |
| Why did you/your team make that choice (justification) when solving the problem? | We all agreed as visiting the company will bring us idea how to have the structure of payload and how do the systems work. We  choose to go on the 3rd week of the semester. |
| What was the impact on you/your team/the project when you make that decision? | We feel by visiting drones company it brings lot of experience and we believe that we will acknowledge something from the visit. |
| What is the next step? | We have been set up the date to visit drones company(aerodyne) and we will propose the idea to other subsystem team. |
