WEEKLY REPORT_ WEEK 4
| Items | DESCRIBTION |
| ------ | ------ |
|Name| Nur Syahirah |
| Matrics No | 197951 |
| Subsystem | Payload Design(Sprayer) |
| Group | 5 |
| Date |  19 November 2021 |

| DESCRIPTION | GROUP 5 | SUBSYSTEM |
| ------ | ------ | ------ | 
| What is our agenda this week and what are our/my goals? |  for week 4, we rotate and take turn the roles such are process monitor, recorder and checker. for me this week my roles is recorder.  | For our subsystem, we had some discussion about considering the design of the three main components which are tank, nozzle sprayer, and pump system. The amount of pesticides also need to be consider due to take care the crops to not have too much amount of pesticides used.  |
| What discussion did you/your team make in solving a problem? |  -  | we discuss with fiqri about the critea that he wants and we confirmed the design to fiqri. there some suggestion to us. we discuss about in fixing the 3 mains components.  |
| How did you/your team make those decisions (method) | -  | we ask opion from fiqri and he helped us so much and many input we got from him. we also review the information that Dr. Salah gives in group. |
| Why did you/your team make that choice (justification) when solving the problem? | - | Everyone gives the ideas on and we discuss what can and what cannot in functioning payload sprayer. |
| What was the impact on you/your team/the project when you make that decision? | - | As the impact, we could achieved final decision on the 3 main components  |
