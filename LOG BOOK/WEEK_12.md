WEEKLY REPORT_ WEEK 12
| Items | DESCRIBTION |
| ------ | ------ |
|Name| Nur Syahirah |
| Matrics No | 197951 |
| Subsystem | Payload Design(Sprayer) |
| Group | 5 |
| Date |  16 January 2022 |

| DESCRIPTION | GROUP 5 | SUBSYSTEM |
| ------ | ------ | ------ | 
|what is our agenda?||our agenda for week 12 is to find the solution for the problems that we faced  such that leakage, pump position, nozzle pressure.|
|What discussion did you/your team make in solving a problem?||	In solving the problem, the discussion that we made for leakage are we discuss want to use flex tape from leaking. also we discuss on how will be the best position for pump to operate. For nozzle pressure, we discuss to try loosen the cap from nozzle so that liquid can flow through it.|
|How did you/your team make those decisions?||We tried few times with a few methods and whenever we saw the best way in solve the problem we finalized with final method.| 
|Why did you/your team make that choice(justification) when solving the problem?||The reason we decide on how to solve the problem of sprayer because after we discuss we run the test on spayer and our theory are acceptable.|
 |What was the impact on you/your team/the project when you make that decision?||As the impact we successfully solving problem that we face before.|

![image](/uploads/47e5d960b3f560ed3b29c4a98d62dab3/image.png)

_Figure 1: Leak proof tape succesfully applied_

![image](/uploads/25d39769595498886b0e32426f0fa9ae/image.png)

_Figure 2: Best position for pump_
