WEEKLY REPORT_ WEEK 13
| Items | DESCRIBTION |
| ------ | ------ |
|Name| Nur Syahirah |
| Matrics No | 197951 |
| Subsystem | Payload Design(Sprayer) |
| Group | 5 |
| Date |  23 January 2022 |

| DESCRIPTION | GROUP 5 | SUBSYSTEM |
| ------ | ------ | ------ | 
|what is our agenda?| |Our agenda is to continue the progress where we need to do an attachment with gondola to see how far they can withstand with the payload and we planned to put with an airship.|
|what dscussion did you/your team make in solving a problem?||We discuss with gondola team to use plywood as plate where it can support gondola. We plan to have L bracket at the of the tank to connect with plate made of plywood. On the bottom of the plywood we plan to have velcro strap in order to attach with the airship.|
|how did you/your team make those decisions?||We make those decision based on the stability that can  be obtain from gondola  and payload sprayer system.|
|how did you/your team make that choice(justification) when solving the problem?||We take an action to try assemble as planned. The result can be seen from the picture that have been provided.|
|what was the impact on you/your team/the project when make that decision?||As the impact our planned is sucessfully attach with gondola.|

![image](/uploads/7bf6f803b1ec6dd6f9ff1b97d83701dc/image.png)

_Figure 1: Payload with velcro strap_

![image](/uploads/3c88147a70b7e61528f30fa316291edb/image.png)

_Figure 2: Payload attachment with plywood_
