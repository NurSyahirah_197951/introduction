WEEKLY REPORT_ WEEK 6
| Items | DESCRIBTION |
| ------ | ------ |
|Name| Nur Syahirah |
| Matrics No | 197951 |
| Subsystem | Payload Design(Sprayer) |
| Group | 5 |
| Date |  26 November 2021 |

| DESCRIPTION | GROUP 5 | SUBSYSTEM |
| ------ | ------ | ------ | 
| What is our agenda this week and what are our/my goals? |  In week 4, we rotate and take turn the roles such are process monitor, recorder and checker. for me this week my roles is checker where my job is to double check the final product, making sure it complete, error free and turned it on time.  | For our subsystem, we had some discussion about design of payload, calculations on the Galloon Per Minute(GPM), types of sprayer nozzles used, and water pump.  |
| What discussion did you/your team make in solving a problem? |  -  | we discuss with fiqri about the design where the gondola will attach with payload sprayer, and water pump used to spray the crops later. He is the one that guide us before we present our result to Dr. Salah.   |
| How did you/your team make those decisions (method) | -  | we ask opion from fiqri and he helped us so much and many input we got from him. we also review the information that Dr. Salah gives in group. |
| Why did you/your team make that choice (justification) when solving the problem? | - | As we doing our task, we sometimes google the information to double check the justification everytime we made. |
| What was the impact on you/your team/the project when you make that decision? | - | As the impact, we could achieved final decision before we present to Dr Salah.  |
