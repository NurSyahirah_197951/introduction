WEEKLY REPORT_ WEEK 8
| Items | DESCRIBTION |
| ------ | ------ |
|Name| Nur Syahirah |
| Matrics No | 197951 |
| Subsystem | Payload Design(Sprayer) |
| Group | 5 |
| Date |  19 December 2021 |

| DESCRIPTION | GROUP 5 | SUBSYSTEM |
| ------ | ------ | ------ | 
|what is our agenda?||Our agenda for week 8 is to achieve the objectives of the meetings such are battery accessibility, power board, stability, weight, thruster arm connector, attachment of payload to airship. All the objectives must be follow in order to fly the airship.|
|what discussion did you/your team make in solving a problem?||To solve the problem we discuss among us what is the best way produce effective quality. We also use drawing board during meeting to see more clearer the see probability of the product.|
  |how did you/your team make those decisions?||To make a decision as usual we hear our groupmate idea and discuss what is the pros and cons if the product produce like that. So we decide to make decision after Dr. Salahuddin and fiqri approve it. We also propose a few of our ideas.|
 |how did you/your team make that choice(justification) when solving the problem?||We only proceed if Dr. Salah and fiqri approve our attachment of payload to the airships and also the position of boom stick to the tank.|
|what was the impact on you/your team/the project when make that decision?||The impact from decision that have been made are mounting design is developed and updated into tracker that we provided before. The estimated weight is calculated and updated. The design was choosen.|

![image](/uploads/96254b7da080e26ec71a99995ba3c50f/image.png)

_Figure 1: Idea attachment on airship_
