WEEKLY REPORT_ WEEK 7
| Items | DESCRIBTION |
| ------ | ------ |
|Name| Nur Syahirah |
| Matrics No | 197951 |
| Subsystem | Payload Design(Sprayer) |
| Group | 5 |
| Date |  3 December 2021 |

| DESCRIPTION | GROUP 5 | SUBSYSTEM |
| ------ | ------ | ------ | 
| What is our agenda this week and what are our/my goals? |  In week 7, we rotate and take turn the roles such are process monitor, recorder and checker. for me this week I got no roles.  | For our subsystem, we had some discussion about finalising the water pump and nozzles. We also had some discussion about how many ml of pesticide used mixing with water. The finalise pesticide have been decide where we will use 3.17ml for 5 litres tank.  |
| What discussion did you/your team make in solving a problem? |  -  |  we discuss on how to mix pesticides with water. we need the ratio of it due to density will effect the mass of pesticides.  |
| How did you/your team make those decisions (method) | -  | we do some ratio of the pesticides and the final calculations we got 3.17ml pesticides for 5 litre tank. |
| Why did you/your team make that choice (justification) when solving the problem? | - | we follow the guide provided from the tables of calculations. |
| What was the impact on you/your team/the project when you make that decision? | - | As the impact, we could achieved final decision of amount of pesticides.  |