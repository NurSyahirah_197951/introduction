WEEKLY REPORT_ WEEK 11
| Items | DESCRIBTION |
| ------ | ------ |
|Name| Nur Syahirah |
| Matrics No | 197951 |
| Subsystem | Payload Design(Sprayer) |
| Group | 5 |
| Date |  9 January 2022 |

| DESCRIPTION | GROUP 5 | SUBSYSTEM |
| ------ | ------ | ------ | 
|what is our agenda?||Our agenda this week is to assembled all sprayer equipments and next test the functionality of pump and nozzles.|
|what dscussion did you/your team make in solving a problem?||Our discussion based on how  we gonna assemble sprayer equipments in order to balance the design. Before we proceed to the real assembling we use the dummy equipment first to test the balancing.|
|how did you/your team make those decisions?||After we assemble all the equipments, we test the sprayer which we hang it to the roof of lab H2.1. after the test was run we decide to assemble sprayer equipments because with that assblem design there was very least problem that we faced and also balanced.|
|how did you/your team make that choice(justification) when solving the problem?||After a few discussion that we had. we choose the problem that have a very least problem so that we can solve faster with given time.|
|what was the impact on you/your team/the project when make that decision?||As impact we realize that our test on dummy equipments produce a great result. The sprayer design was a successful decision.|

![image](/uploads/7da59b79b614aa958249a0d41891ea87/image.png)

_Figure 1: Sprayer nozzles test_

![image](/uploads/5f78b1694bbd1a88826b5640e9efc8ef/image.png)

_Figure 2: Arrangement of nozzles_

![image](/uploads/f5e2aa231e2a6163bfe26809eccd878c/image.png)

_Figure 3: Attachment of pump to dummy tank_
